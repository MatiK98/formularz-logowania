import React, { useState } from 'react';
import "../style.css/App.css";
import { AiOutlineMail } from 'react-icons/ai';
import { BiLockAlt } from 'react-icons/bi';
import { AiFillFacebook } from 'react-icons/ai';
import { FcGoogle } from 'react-icons/fc';

const LoginForm = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleEmail = (e) => setEmail(e.target.value)
    const handlePassword = (e) => setPassword(e.target.value);

    const submitForm = (e) => {
        e.preventDefault();

        if (email.length <= 7) {
            alert("Login musi mieć minimum 8 znaków!");
            return;
        }
        if (password.length <= 7 || password.search(/[A-Z]/) === -1 || password.search(/(?=.*[!@#$%^&*])/) !== -1) {
            alert("Hasło musi mieć minium 8 znaków, zawierać przynajmniej jedną wielką literę oraz nie zawierać znaków specjalnych!");
            return;
        }

        else {
            setEmail("");
            setPassword("");
        }
    }



    return (
        <div className="wrapper">
            <h1 className="login">login</h1>
            <form>
                <AiOutlineMail className="emailIcon" />
                <input type="text" className="email" placeholder="Email" value={email} onChange={handleEmail} />
                <br />
                <BiLockAlt icon="lock" className="passwordIcon" />
                <input type="password" className="password" placeholder="Password" value={password} onChange={handlePassword} />
                <br />
                <p className="checkbox"><input type="checkbox" className="checkInput" /> <span className="text">Remember me</span></p>
                <button type="submit" className="submitButton" onClick={submitForm}>login</button>
            </form>
            <section>
                <h3 className="optionLogin">Or login with</h3>
                <div className="socials">
                    <span className="facebook"><AiFillFacebook className="facebookIcon" /> Facebook</span>
                    <span className="google"><FcGoogle className="googleIcon" /> Google</span>
                </div>
            </section>
            <footer className="footer">Not a member? <span className="signUp">Sign up now</span></footer>
        </div>
    )
}

export default LoginForm;
